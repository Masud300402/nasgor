  read()

   function read(){
    $.ajax({
      url: site_url + 'barang/read',
      type:'POST',
      success:function(data){
        $('tbody').html(data);
        $('.table').footable({
          "paging": {
            "enabled": true
          },
          "filtering": {
            "enabled": true
          },
          "sorting": {
            "enabled": true
          }
        });
      }
    })
  }


  $('#form-add').submit(function(e){
    e.preventDefault();
    var data = $('#form-add').serialize();
    $.ajax({
      url: site_url + 'barang/save/tambah/',
      type:"POST",
      data:data,
      success:function(data){
        $('#tambah').modal('hide');
        $('#kode').val(data);
        read();
      }
    })
  })

  $(document).on('click','#edit',function(e){
    var id   = $(this).data('id');
    $.ajax({
      url:site_url + 'barang/save/load/'+id,
      type:"POST",
      dataType:"JSON",
      success:function(data){
        $('.modal-edit #id').val(id);
        $('.modal-edit #kode').val(data.kode);
        $('.modal-edit #barcode').val(data.barcode);
        $('.modal-edit #nama').val(data.name);
        $('.modal-edit #satuan').val(data.satuan);
        $('.modal-edit #jenis').val(data.jenis);
        $('.modal-edit #stok').val(data.stok);
        $('.modal-edit #harga').val(data.harga);
      }
    })
  })

  $('#form-edit').submit(function(e){
    e.preventDefault();
    var data = $('#form-edit').serialize();
    var id = $('#id').val();
    $.ajax({
      url:site_url + 'barang/save/edit/' + id ,
      type:"POST",
      data:data,
      success:function(){
        $('#update').modal('hide');
        read();
      }
    })
  })

  function hapus(id){
   swal({
    title: "Are you sure?",
    text: "You will not be able to recover this imaginary file!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it",
    cancelButtonText: "No, cancel",
    closeOnConfirm: false,
    closeOnCancel: false
  }, function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        url : site_url + 'barang/save/hapus/'+id,
        type : 'POST',
        cache: false,
        contentType: false,
        processData: false,
        success:function(){

          swal({
            title: "Deleted!",
            text: "Your imaginary file has been deleted.",
            type: "success",
          }, function (isConfirm) {
            read();
          });

        }
      });
    } else {
      swal("Cancelled", "Your imaginary file is safe :)", "error");
    }
  });
 }
 $(document).ready(function(){
  $('#page').on('change', function(e){
    e.preventDefault();
    var newSize = $(this).val();
    FooTable.get('.table').pageSize(newSize);
  });
})