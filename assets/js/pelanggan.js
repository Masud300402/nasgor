  $(document).ready(function(){
    


    $('#form-add').submit(function(e){
      e.preventDefault();
      var data = $('#form-tambah').serialize();
      $.ajax({
        url: base_url + 'pelanggan/save/tambah/',
        type:"POST",
        data:data,
        success:function(data){
          $('#form-add').modal('hide');
          read();
          document.getElementById('form-tambah').reset()
          $('#kode').val(data);
        }
      })
    })


    $(document).on('click','#edit',function(){
      var id   = $(this).data('id');

      $.ajax({
        url:base_url +'pelanggan/save/load/' + id,
        type:"POST",
        dataType:"JSON",
        success:function(data){
          $('.modal-edit #kode').val(data.kode);
          $('.modal-edit #id').val(id);
          $('.modal-edit #nama').val(data.nama);
          $('.modal-edit #alamat').val(data.alamat);
          $('.modal-edit #toko').val(data.toko);
        }
      })

    })

    $('#form-update').submit(function(e){
      e.preventDefault();
      var data = $('#form-update').serialize();
      var id = $('#id').val();
      $.ajax({
        url: site_url + 'pelanggan/save/edit/'+id,
        type:"POST",
        data:data,
        success:function(){
          read();
          $('#form-edit').modal('hide');
        }
      })
    })

   

   $(document).ready(function(){
    $('#page').on('change', function(e){
      e.preventDefault();
      var newSize = $(this).val();
      FooTable.get('.table').pageSize(newSize);
    });
  })
 })
  read()

    function read(){
      $.ajax({
        url:base_url + 'pelanggan/Read',
        type:'POST',
        success:function(data){
          $('tbody').html(data);
          $('.table').footable({
            "paging": {
              "enabled": true
            },
            "filtering": {
              "enabled": true
            },
            "sorting": {
              "enabled": true
            }
          });
        }
      })
    }

   function hapus(id){
     swal({
      title: "Are you sure?",
      text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it",
      cancelButtonText: "No, cancel",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          url : site_url + 'pelanggan/save/hapus/'+id,
          type : 'POST',
          cache: false,
          contentType: false,
          processData: false,
          success:function(){

            swal({
              title: "Deleted!",
              text: "Your imaginary file has been deleted.",
              type: "success",
            }, function (isConfirm) {
              read();
            });

          }
        });
      } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    });
   }