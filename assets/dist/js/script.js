$(document).ajaxStart(function(){
  $.toast({
    text: "Dalam Proses harap Tunggu", 
    heading: 'Perhatian', 
    icon: 'warning',
    showHideTransition: 'fade',
    allowToastClose: true,
    stack: 5,
    position: 'top-right', 
    textAlign: 'left',  
    loader: true, 
    loaderBg: '#9EC600', 
  });
})

$(document).ajaxSuccess(function(){
  $.toast().reset('all');
  $.toast({
    text: "Sukses ", 
    heading: 'Perhatian', 
    icon: 'success', 
    showHideTransition: 'fade', // f
    allowToastClose: true, 
    hideAfter: 1000, 
    stack: 5, 
    position: 'top-right', 
    textAlign: 'left',  
    loader: true,  
    loaderBg: '#9EC600', 
  });
})

$(document).ajaxError(function(){
  $.toast().reset('all');
  $.toast({
    text: "Error ", 
    heading: 'Perhatian', 
    icon: 'error', 
    showHideTransition: 'fade', // f
    allowToastClose: true, 
    hideAfter: 3000, 
    stack: 5, 
    position: 'top-right', 
    textAlign: 'left',  
    loader: true,  
    loaderBg: '#9EC600', 
  });
})