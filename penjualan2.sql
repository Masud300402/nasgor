-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 27 Mar 2019 pada 08.00
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penjualan2`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `tbl_transaksi_detail_insert` (IN `pfood_id` INT, IN `pno_pesanan` VARCHAR(50), IN `pjumlah` INT, IN `psub_total` DECIMAL(10,0), IN `pcreated_by` VARCHAR(50))  BEGIN
	INSERT INTO tbl_transaksi_detail(food_id,no_pesanan,jumlah,sub_total,created_by) values(pfood_id,pno_pesanan,pjumlah,psub_total,pcreated_by);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `transaksi_insert` (IN `pnopesanan` VARCHAR(50), IN `ppelanggan` INT, IN `pdriver` INT, IN `ptgltransaksi` DATETIME, IN `ptotal` DECIMAL(10,0), IN `pjumlah` INT, IN `puang_bayar` DECIMAL(10,0), IN `puang_kembali` DECIMAL(10,0), IN `pcreated_by` VARCHAR(50), IN `pketerangan` TEXT)  BEGIN
 insert into tbl_transaksi(no_pesanan,pelanggan_id,driver_id,tgl_transaksi,total,jumlah,uang_bayar,uang_kembali,created_by,keterangan)
 values(pnopesanan,ppelanggan,pdriver,ptgltransaksi,ptotal,pjumlah,puang_bayar,puang_kembali,pcreated_by,pketerangan);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_driver`
--

CREATE TABLE `tbl_driver` (
  `id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT '0',
  `no_hp` varchar(50) DEFAULT '0',
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_driver`
--

INSERT INTO `tbl_driver` (`id`, `code`, `name`, `no_hp`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(12, 'D000001', 'Mas\'ud', '082233472254', '2019-03-25 23:52:32', 'admin', '2019-03-25 23:52:32', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_food`
--

CREATE TABLE `tbl_food` (
  `id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT '0',
  `name` varchar(50) DEFAULT '0',
  `harga` decimal(10,0) DEFAULT '0',
  `jumlah` varchar(50) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_food`
--

INSERT INTO `tbl_food` (`id`, `code`, `name`, `harga`, `jumlah`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(2, 'F000003', 'Nasi Goreng', '599000', '1', '2019-03-26 00:05:03', 'admin', '2019-03-26 00:12:56', 'admin'),
(3, 'F000004', 'Nasi  Goreng Spesial', '12', '1', '2019-03-26 00:15:45', 'admin', '2019-03-26 00:15:45', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_group_users`
--

CREATE TABLE `tbl_group_users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_group_users`
--

INSERT INTO `tbl_group_users` (`id`, `name`, `created`, `modified`) VALUES
(1, 'SuperAdmin', '2019-03-23 21:21:25', '2019-03-23 21:21:25'),
(2, 'admin', '2019-03-23 21:21:38', '2019-03-23 21:21:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL,
  `menu_parent_id` int(11) DEFAULT '0',
  `menu_name` varchar(255) NOT NULL,
  `menu_url` varchar(255) NOT NULL,
  `menu_desc` varchar(50) DEFAULT '0',
  `menu_icon` varchar(50) DEFAULT '0',
  `menu_parent` enum('Yes','No') DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_menu`
--

INSERT INTO `tbl_menu` (`id`, `menu_parent_id`, `menu_name`, `menu_url`, `menu_desc`, `menu_icon`, `menu_parent`, `created`, `modified`, `updated_date`, `updated_by`, `deleted_date`, `deleted_by`) VALUES
(1, 0, 'Dashboard', 'Dashboard', 'Dashboard', 'home', 'No', '2019-03-23 21:30:15', '2019-03-23 22:30:35', NULL, NULL, NULL, NULL),
(2, 0, 'Master', 'master', 'master', 'database', 'Yes', '2019-03-23 22:27:05', '2019-03-23 22:35:38', NULL, NULL, NULL, NULL),
(4, 2, 'Users', 'users', 'Users', 'circle-o', 'No', '2019-03-23 22:44:09', '2019-03-25 23:05:16', NULL, NULL, NULL, NULL),
(5, 0, 'Transaksi', 'transaksi', 'transaksi', 'shopping-cart', 'No', '2019-03-25 10:43:29', '2019-03-25 10:43:35', NULL, NULL, NULL, NULL),
(6, 2, 'Makanan', 'food', 'Makanan', 'circle-o', 'No', '2019-03-25 13:57:11', '2019-03-25 23:05:14', NULL, NULL, NULL, NULL),
(7, 2, 'Driver', 'Driver', 'Driver', 'circle-o', 'No', '2019-03-25 23:01:44', '2019-03-25 23:05:44', NULL, NULL, NULL, NULL),
(8, 2, 'Pelanggan', 'Pelanggan', 'Pelanggani', 'circle-o', 'No', '2019-03-25 23:45:34', '2019-03-27 09:32:00', NULL, NULL, NULL, NULL),
(9, 0, 'Laporan', 'Laporan', 'Laporan', 'book', 'No', '2019-03-27 11:12:34', '2019-03-27 11:12:44', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_menu_role`
--

CREATE TABLE `tbl_menu_role` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) DEFAULT '0',
  `group_users_id` int(11) DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `deleted_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_menu_role`
--

INSERT INTO `tbl_menu_role` (`id`, `menu_id`, `group_users_id`, `created`, `modified`, `updated_date`, `updated_by`, `deleted_date`, `deleted_by`) VALUES
(19, 1, 1, '2019-03-26 10:19:43', '2019-03-26 10:20:22', NULL, NULL, NULL, NULL),
(21, 1, 2, '2019-03-26 10:19:53', '2019-03-26 10:19:53', NULL, NULL, NULL, NULL),
(22, 7, 2, '2019-03-26 10:23:04', '2019-03-26 10:23:04', NULL, NULL, NULL, NULL),
(23, 7, 1, '2019-03-26 10:23:04', '2019-03-26 10:23:10', NULL, NULL, NULL, NULL),
(24, 6, 2, '2019-03-26 10:23:17', '2019-03-26 10:23:17', NULL, NULL, NULL, NULL),
(25, 6, 1, '2019-03-26 10:23:17', '2019-03-26 10:23:22', NULL, NULL, NULL, NULL),
(26, 2, 2, '2019-03-26 10:23:31', '2019-03-26 10:23:31', NULL, NULL, NULL, NULL),
(27, 2, 1, '2019-03-26 10:23:31', '2019-03-26 10:23:37', NULL, NULL, NULL, NULL),
(28, 8, 2, '2019-03-26 10:23:31', '2019-03-26 10:23:45', NULL, NULL, NULL, NULL),
(29, 8, 1, '2019-03-26 10:23:31', '2019-03-26 10:23:52', NULL, NULL, NULL, NULL),
(30, 5, 2, '2019-03-26 10:24:00', '2019-03-26 10:24:00', NULL, NULL, NULL, NULL),
(32, 5, 1, '2019-03-26 10:24:11', '2019-03-26 10:24:11', NULL, NULL, NULL, NULL),
(33, 4, 2, '2019-03-26 10:24:25', '2019-03-26 10:24:25', NULL, NULL, NULL, NULL),
(34, 4, 1, '2019-03-26 10:24:33', '2019-03-26 10:24:33', NULL, NULL, NULL, NULL),
(35, 9, 2, '2019-03-27 11:13:00', '2019-03-27 11:13:00', NULL, NULL, NULL, NULL),
(36, 9, 1, '2019-03-27 11:13:00', '2019-03-27 11:13:05', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pelanggan`
--

CREATE TABLE `tbl_pelanggan` (
  `id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT '0',
  `no_hp` varchar(50) DEFAULT '0',
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_pelanggan`
--

INSERT INTO `tbl_pelanggan` (`id`, `code`, `name`, `no_hp`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(2, 'P000001', 'Mas\'ud', '082233472254', '2019-03-25 23:54:42', 'admin', '2019-03-25 23:54:42', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_transaksi`
--

CREATE TABLE `tbl_transaksi` (
  `id` int(11) NOT NULL,
  `no_pesanan` varchar(50) DEFAULT '0',
  `pelanggan_id` int(11) DEFAULT '0',
  `driver_id` int(11) DEFAULT '0',
  `tgl_transaksi` date DEFAULT NULL,
  `total` decimal(10,0) DEFAULT '0',
  `jumlah` int(11) DEFAULT '0',
  `uang_bayar` decimal(10,0) DEFAULT '0',
  `uang_kembali` decimal(10,0) DEFAULT '0',
  `keterangan` text,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_transaksi`
--

INSERT INTO `tbl_transaksi` (`id`, `no_pesanan`, `pelanggan_id`, `driver_id`, `tgl_transaksi`, `total`, `jumlah`, `uang_bayar`, `uang_kembali`, `keterangan`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(7, 'F-00004', 0, 0, '2019-03-27', '12', 1, '30', '-18', NULL, '2019-03-27 10:56:55', 'Husada', '2019-03-27 10:56:55', NULL),
(8, '', 0, 0, '0000-00-00', '0', 0, '0', '0', NULL, '2019-03-27 11:12:05', '', '2019-03-27 11:12:05', NULL),
(9, 'F-00024', 2, 12, '2019-03-27', '12', 1, '11', '1', 'masuk', '2019-03-27 11:29:48', 'Husada', '2019-03-27 11:29:48', NULL),
(10, 'f0000121221', 0, 0, '2019-03-27', '599000', 1, '600000', '-1000', 'asasasasa', '2019-03-27 13:07:56', 'Husada', '2019-03-27 13:07:56', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_transaksi_detail`
--

CREATE TABLE `tbl_transaksi_detail` (
  `id` int(11) NOT NULL,
  `food_id` int(11) DEFAULT '0',
  `no_pesanan` varchar(50) DEFAULT '0',
  `jumlah` int(11) DEFAULT '0',
  `sub_total` decimal(10,0) DEFAULT '0',
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_transaksi_detail`
--

INSERT INTO `tbl_transaksi_detail` (`id`, `food_id`, `no_pesanan`, `jumlah`, `sub_total`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(43, 3, 'F-00004', 1, '12', '2019-03-27 10:56:55', 'Husada', '2019-03-27 10:56:55', NULL),
(44, 3, 'F-00024', 1, '12', '2019-03-27 11:29:48', 'Husada', '2019-03-27 11:29:48', NULL),
(45, 2, 'f0000121221', 1, '599000', '2019-03-27 13:07:56', 'Husada', '2019-03-27 13:07:56', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_transaksi_sementara`
--

CREATE TABLE `tbl_transaksi_sementara` (
  `id` int(11) NOT NULL,
  `food_id` int(11) DEFAULT '0',
  `jumlah` int(11) DEFAULT '0',
  `sub_total` decimal(10,0) DEFAULT '0',
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT '0',
  `password` varchar(50) DEFAULT '0',
  `name` varchar(50) DEFAULT '0',
  `group_users_id` int(11) DEFAULT '0',
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `username`, `password`, `name`, `group_users_id`, `created_date`, `created_by`, `updated_date`, `updated_by`, `deleted_date`, `deleted_by`) VALUES
(6, 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'superadmin', 1, '2019-03-23 21:43:36', 'superadmin', '2019-03-25 14:00:27', NULL, '2019-03-25 14:00:27', NULL),
(7, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Husada', 2, '2019-03-23 21:43:49', 'Superadmin', '2019-03-26 08:24:38', 'superadmin', '2019-03-26 08:24:38', NULL),
(13, 'mawan', 'a2bd3bdf0251c547c4904205d927f4cc', 'Mawan', 1, '2019-03-26 10:13:47', 'Husada', '2019-03-26 10:13:47', NULL, '2019-03-26 10:13:47', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_driver`
--
ALTER TABLE `tbl_driver`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_food`
--
ALTER TABLE `tbl_food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_group_users`
--
ALTER TABLE `tbl_group_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_menu_role`
--
ALTER TABLE `tbl_menu_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`) USING BTREE,
  ADD KEY `group_users_id` (`group_users_id`) USING BTREE;

--
-- Indexes for table `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transaksi_detail`
--
ALTER TABLE `tbl_transaksi_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transaksi_sementara`
--
ALTER TABLE `tbl_transaksi_sementara`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_tbl_transaksi_sementara_tbl_food` (`food_id`) USING BTREE;

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_user_id` (`group_users_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_driver`
--
ALTER TABLE `tbl_driver`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_food`
--
ALTER TABLE `tbl_food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_group_users`
--
ALTER TABLE `tbl_group_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_menu_role`
--
ALTER TABLE `tbl_menu_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tbl_pelanggan`
--
ALTER TABLE `tbl_pelanggan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_transaksi`
--
ALTER TABLE `tbl_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbl_transaksi_detail`
--
ALTER TABLE `tbl_transaksi_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `tbl_transaksi_sementara`
--
ALTER TABLE `tbl_transaksi_sementara`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_menu_role`
--
ALTER TABLE `tbl_menu_role`
  ADD CONSTRAINT `FK_tbl_menu_role_tbl_group_users` FOREIGN KEY (`group_users_id`) REFERENCES `tbl_group_users` (`id`),
  ADD CONSTRAINT `FK_tbl_menu_role_tbl_menu` FOREIGN KEY (`menu_id`) REFERENCES `tbl_menu` (`id`);

--
-- Ketidakleluasaan untuk tabel `tbl_transaksi_sementara`
--
ALTER TABLE `tbl_transaksi_sementara`
  ADD CONSTRAINT `FK_tbl_transaksi_sementara_tbl_food` FOREIGN KEY (`food_id`) REFERENCES `tbl_food` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
