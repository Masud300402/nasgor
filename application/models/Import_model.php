<?php 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class Import_model extends CI_Model {
 
    private $_batchImport;
 
    public function setBatchImport($batchImport) {
        $this->_batchImport = $batchImport;
    }
 
    // save data
    public function importData($tbl) {
        $data = $this->_batchImport;
        $this->db->insert_batch($tbl, $data);
    }
    // get employee list
    public function employeeList() {
        $this->db->select(array('e.id', 'e.name', 'e.code', 'e.no_hp'));
        $this->db->from('tbl_driver as e');
        $query = $this->db->get();
        return $query->result_array();
    }
 
}