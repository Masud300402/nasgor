<?php 
defined("BASEPATH")or exit('NO DIRECT SCRIPT ALLOWED');

class Driver extends CI_Controller{

	public function index(){
		cek_session();
		$this->session->unset_userdata('barkode');
		$db['title'] = "Driver";
		$this->load->helper('form');
		LoadView('driver','index.php',$db);
	}

	public function read(){
		$query = $this->db->get('tbl_driver')->result_array();
		$no = 1;
		foreach ($query as $key) {
			?>
			<tr>
				
				<td width="250" id="opsi">
					<div class="btn-group">
						<button 
						data-id='<?php echo $key['id'] ?>' data-toggle='modal' data-target='#form-edit' type="button" id="edit" class="btn btn-warning btn-sm" ><i class="fa fa-edit"></i>&nbsp&nbspEdit Data</button>
						<button onclick="hapus(<?php echo $key['id'] ?>)" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp&nbspHapus Data</button>
					</div>
					
				</td>
				<td><?php echo $no++ ?></td>
				<td><a href="<?php echo base_url('laporan?driver=').$key["code"]?>"><?php echo $key['code'] ?></a></td>
				<td><?php echo $key['name'] ?></td>
				<td><?php echo $key['no_hp'] ?></td>
				
			</tr>
			<?php
		}

	}

	public function save(){
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$post = $this->input->post();
		$nama = $this->session->userdata('nama');
		if ($uri3 == 'tambah') {
			$this->name           = $post['nama'];
			$this->no_hp          = $post['hp'];
			$this->code           = $post['kode'];
			$this->created_by     = $nama;
			$this->db->insert('tbl_driver',$this);
			echo  kode('tbl_driver','D');
		}
		if ($uri3 == 'edit') {
			$this->name           = $post['nama'];
			$this->no_hp          = $post['hp'];
			$this->code           = $post['kode'];
			$this->updated_by     = $nama;
			$this->db->where(array('id' => $uri4 ));
			$this->db->update('tbl_driver',$this);
			echo  kode('tbl_driver','D');

		}
		if ($uri3 == 'hapus') {
			$this->db->where(array('id' => $uri4 ));
			$this->db->delete('tbl_driver');	
			echo  kode('tbl_driver','D');
		}
		if ($uri3 == 'load') {
			$sql = $this->db->get_where('tbl_driver',array('id'=>$uri4))->result_array();

			foreach ($sql as $key) {
				$name           = $key['name'];
				$id             = $key['id'];
				$no_hp          = $key['no_hp'];
				$code           = $key['code'];		
			}
			echo json_encode(array('id'=>$id,'nama'=>$name,'no_hp'=>$no_hp,'kode'=>$code));
		}

		
	}

}