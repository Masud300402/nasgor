<?php 
defined("BASEPATH")or exit('NO DIRECT SCRIPT ALLOWED');

class Login extends Ci_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
	}
	public function index(){
		if (!$this->session->userdata('username')) {

			$this->load->view('login/index.php');


		}else{
			redirect('dashboard');
		}


	}


	public function cek_login()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|trim');	
		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		if ($this->form_validation->run() == false ) {

			redirect();


		} else {

			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));

			$query = $this->db->query('SELECT u.id,u.name,u.username,u.password,u.group_users_id from tbl_users u where username = "'.$username.'" AND password = "'.$password.'" ');

			if ($query->num_rows() == 1) {
				foreach ($query->result() as $row) {
					$sess = array(
						'username' => $row->username,
						'id' => $row->id,
						'nama' => $row->name,
						'role' => $row->group_users_id
					);
					$this->session->set_userdata($sess);
				}
				redirect('dashboard');
			} 
			else{
				$this->session->set_flashdata('info', 'Invalid Username and Password');
				redirect();
			} 
		}   
	}

	public function logout(){
		$this->session->unset_userdata('username');
		redirect('login');
	}
}