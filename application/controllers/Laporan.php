<?php 
defined("BASEPATH")or exit('NO DIRECT SCRIPT ALLOWED');

class Laporan extends CI_Controller{

	public function index()
	{
		cek_session();
		$db['title'] = 'Laporan';
		if (isset($_GET['pelanggan'])) {
			$db['pelanggan'] = $_GET['pelanggan'];
		}
		if (isset($_GET['driver'])) {
			$db['driver']   =  $_GET['driver'];
		}
		if (isset($_GET['tanggal'])) {
			$db['tanggal']   =  $_GET['tanggal'];
		}
		echo loadview('laporan','index.php',$db);
	}

	public function read(){
		$where = '';
		if (isset($_GET['pelanggan'])) {
			$where = 'WHERE p.code = "'.$_GET['pelanggan'].'"';
		}
		if (isset($_GET['driver'])) {
			$where = 'WHERE d.code = "'.$_GET['driver'].'"';
		}
		if (isset($_GET['tanggal'])) {
			$where = 'WHERE t.tgl_transaksi = "'.$_GET['tanggal'].'"';
		}

		$transaksi  = $this->db->query("SELECT t.no_pesanan,p.name as pnama,d.name as dnama,t.tgl_transaksi,t.total,t.jumlah,t.uang_bayar as bayar,t.uang_kembali as kembali from tbl_transaksi t LEFT JOIN tbl_pelanggan p on p.id = t.pelanggan_id LEFT JOIN tbl_driver d on d.id = t.driver_id $where order by t.id desc")->result_array();
		$no = 1;
		foreach ($transaksi as $key ){
			?>
			<tr>
				<td><?php echo $no++ ?></td>
				<td><?php echo $key['no_pesanan']; ?></td>
				<td><?php echo $key['pnama']; ?></td>
				<td><?php echo $key['dnama']; ?></td>
				<td><?php echo $key['tgl_transaksi']; ?></td>
				<td><?php echo number_format($key['total']) ?></td>
				<td><?php echo $key['jumlah']; ?></td>
				<td><?php echo number_format($key['bayar']); ?></td>
				<td><?php echo number_format($key['kembali']); ?></td>
				<td><a href="<?php echo site_url('Laporan/show_barang?np=').$key['no_pesanan'] ?>">Rincian Barang</a></td>
			</tr>

			<?php
		}


	}

	public function show_barang(){
		$np = $_GET['np'];
		$db['query'] = $this->db->query('SELECT f.NAME AS nama,td.no_pesanan,f.harga_jual AS harga,td.jumlah,td.sub_total FROM tbl_transaksi_detail td LEFT JOIN tbl_food f ON f.id = td.food_id WHERE no_pesanan =  "'.$np.'" ')->result_array();
		$db['query2']= $this->db->query('SELECT t.jumlah,t.total,t.uang_bayar ,t.uang_kembali ,t.no_pesanan,t.tgl_transaksi,t.keterangan FROM tbl_transaksi t WHERE no_pesanan  = "'.$np.'" ')->row_array();
		$db['title'] = 'Lihat Barang';
		echo view2('show_barang',$db);
	}


}