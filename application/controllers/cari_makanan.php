<?php 
defined("BASEPATH") or exit('NO DIRECT SCRIPT ALLOWED');
 
 class cari_makanan extends CI_Controller{

 	public function index(){
 		cek_session();
 		$this->session->unset_userdata('barkode');
 		$db['title'] = 'Cari Barang';
 		$db['barang']  = $this->db->get('tbl_food')->result_array();
 		echo view2('cari_makanan',$db);
 	}

 	public function kode(){
    		 $this->session->unset_userdata('barkode');
 		     $kode = $this->input->post('data');
	 		 $sess = array(
						'kode' => $kode
					);
			$this->session->set_userdata($sess);

 
 	}
 }