<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	public function index()
	{
		cek_session();
		$this->load->helper('chart');
		$db['title'] = 'Dashboard';
		$db['Today'] = $this->db->query('SELECT SUM(jumlah) AS today FROM tbl_transaksi WHERE tgl_transaksi = DATE_FORMAT(CURDATE(),"%Y-%m-%d")')->row_array();
		$db['nToday'] = $this->db->query('SELECT SUM(total) AS ntoday FROM tbl_transaksi WHERE tgl_transaksi = DATE_FORMAT(CURDATE(),"%Y-%m-%d")')->row_array();
		LoadView('dashboard','index',$db);
	}

	public function chart(){
		$CI =& get_instance();
		$tahun = date('Y');
		$bulan = date('m');
		for ($i=1; $i <= 31  ; $i++) { 
			$sql ='SELECT IFNULL(SUM(total),"0") AS count from tbl_transaksi WHERE 
			MONTH(tgl_transaksi) = '.$bulan.' AND YEAR(tgl_transaksi) = "'.$tahun.'" AND DAY(tgl_transaksi) = "'.$i.'";  ';
			$query = $CI->db->query($sql)->row_array();
			echo str_replace('"','',json_encode($query['count']).",");
		}


	}

}
