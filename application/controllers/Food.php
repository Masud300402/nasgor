<?php 
defined("BASEPATH")or exit('NO DIRECT SCRIPT ALLOWED');

class Food extends CI_Controller{

	public function index(){
		cek_session();
		$this->session->unset_userdata('barkode');
		$db['title'] = "Food";
		$this->load->helper('form');
		LoadView('food','index.php',$db);
	}

	public function read(){
		$query = $this->db->get('tbl_food')->result_array();
		$no = 1;
		foreach ($query as $key) {
			?>
			<tr>
				<td width="250">
					<div class="btn-group">	
						<button 
						data-id='<?php echo $key['id'] ?>' data-toggle='modal' data-target='#form-edit' type="button" id="edit" class="btn btn-warning btn-sm" ><i class="fa fa-edit"></i>&nbsp&nbspEdit Data</button>
						<button onclick="hapus(<?php echo $key['id'] ?>)" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp&nbspHapus Data</button>
					</div>
				</td>
				<td><?php echo $no++ ?></td>
				<td><?php echo $key['code'] ?></td>
				<td><?php echo $key['name'] ?></td>
				<td><?php echo number_format($key['harga_beli']) ?></td>
				<td><?php echo number_format($key['harga_jual']) ?></td>
				<td><?php echo $key['jumlah'] ?></td>
				
			</tr>
			<?php
		}

	}

	public function save(){
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$post = $this->input->post();
		$nama = $this->session->userdata('nama');
		if ($uri3 == 'tambah') {
			$this->name           = $post['nama'];
			$this->harga_jual     = str_replace(',', '', $post['hargajual']);
			$this->harga_beli     = str_replace(',', '', $post['hargabeli']);
			$this->jumlah         = $post['jumlah'];
			$this->code           = $post['kode'];
			$this->created_by     = $nama;
			$this->db->insert('tbl_food',$this);
			
		}
		if ($uri3 == 'edit') {
			$this->name           = $post['nama'];
			$this->harga_jual     = str_replace(',', '', $post['hargajual']);
			$this->harga_beli     = str_replace(',', '', $post['hargabeli']);
			$this->jumlah         = $post['jumlah'];
			$this->code           = $post['kode'];
			$this->updated_by     = $nama;
			$this->db->where(array('id' => $uri4 ));
			$this->db->update('tbl_food',$this);
			
		}
		if ($uri3 == 'hapus') {
			$this->db->where(array('id' => $uri4 ));
			$this->db->delete('tbl_food');	
		
		}
		if ($uri3 == 'load') {
			$sql = $this->db->get_where('tbl_food',array('id'=>$uri4))->result_array();

			foreach ($sql as $key) {
				$name           = $key['name'];
				$id             = $key['id'];
				$hargajual      = $key['harga_jual'];
				$hargabeli      = $key['harga_beli'];
				$jumlah         = $key['jumlah'];		
				$code           = $key['code'];		
			}
			echo json_encode(array('id'=>$id,'nama'=>$name,'hargajual'=>$hargajual,'hargabeli'=>$hargabeli,'jumlah'=>$jumlah,'code'=>$code));
		}

		
	}
}