<?php 
defined("BASEPATH")or exit('NO DIRECT SCRIPT ALLOWED');

class Users extends CI_Controller{

	public function index(){
		cek_session();
		$this->session->unset_userdata('barkode');
		$db['title'] = "Users";
		$db['group'] = $this->db->get('tbl_group_users')->result_array();
		LoadView('users','index.php',$db);
	}

	public function read(){
		$query = $this->db->query('SELECT u.id,u.name,u.username,g.name as role from tbl_users u LEFT JOIN tbl_group_users g on g.id = u.group_users_id')->result_array();
		$no = 1;
		foreach ($query as $key) {
			?>
			<tr>
				<td>
					<div class="btn-group">	
						<button 
						data-id='<?php echo $key['id'] ?>' data-toggle='modal' data-target='#form-edit' type="button" id="edit" class="btn btn-warning" ><i class="fa fa-edit"></i></button>
						<button onclick="hapus(<?php echo $key['id'] ?>)" type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
					</div>
				</td>
				<td><?php echo $no++ ?></td>
				<td><?php echo $key['name'] ?></td>
				<td><?php echo $key['username'] ?></td>
				<td><?php echo $key['role'] ?></td>
				
			</tr>
			<?php
		}

	}

	public function save(){
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$post = $this->input->post();
		$nama = $this->session->userdata('nama');
		if ($uri3 == 'tambah') {
			$this->name           = $post['nama'];
			$this->username       = $post['username'];
			$this->password       = md5($post['password']);
			$this->group_users_id = $post['group'];
			$this->created_by     = $nama;
			$this->db->insert('tbl_users',$this);
		}
		if ($uri3 == 'edit') {
			$this->name           = $post['nama'];
			$this->username       = $post['username'];
			$this->password       = md5($post['password']);
			$this->group_users_id = $post['group'];
			$this->updated_by     = $nama;
			$this->db->where(array('id' => $uri4 ));
			$this->db->update('tbl_users',$this);

		}
		if ($uri3 == 'hapus') {
			$this->db->where(array('id' => $uri4 ));
			$this->db->delete('tbl_users');	
		}
		if ($uri3 == 'load') {
			$sql = $this->db->query('
				SELECT u.id,u.name,u.username,g.name as role from tbl_users u LEFT JOIN tbl_group_users g on g.id = u.group_users_id where u.id = "'.$uri4.'" '
			)->result_array();

			foreach ($sql as $key) {
				$id = $key['id'];
				$name = $key['name'];
				$username = $key['username'];
				$role = $key['role'];
		
			}
			echo json_encode(array('id'=>$id,'nama'=>$name,'username'=>$username,'role'=>$role));
		}

		
	}
}