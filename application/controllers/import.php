<?php 
defined('BASEPATH') or exit('NO DIRECT SCRIPT ALLOWED');

class import extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->library('Excel');
		$this->load->model('Import_model', 'import');
	}

	public function importDriver(){
		

		if ($this->input->post('importDriver')) {
			$path = 'upload/excel/';
			$config['upload_path'] = $path;
			$config['allowed_types'] = 'xlsx|xls|jpg|png';
			$config['remove_spaces'] = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('importfile')) {
				$error = array('error' => $this->upload->display_errors());
			} else {
				$data = array('upload_data' => $this->upload->data());
			}
			if (!empty($data['upload_data']['file_name'])) {
				$import_xls_file = $data['upload_data']['file_name'];
			} else {
				$import_xls_file = 0;
			}
			$inputFileName = $path . $import_xls_file;
			try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
					. '": ' . $e->getMessage());
			}
			$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

			$arrayCount = count($allDataInSheet);
			$flag = 0;
			$createArray = array('code', 'name', 'no_hp');
			$makeArray = array('code' => 'code', 'name' => 'name', 'no_hp' => 'no_hp');
			$SheetDataKey = array();
			foreach ($allDataInSheet as $dataInSheet) {
				foreach ($dataInSheet as $key => $value) {
					if (in_array(trim($value), $createArray)) {
						$value = preg_replace('/\s+/', '', $value);
						$SheetDataKey[trim($value)] = $key;
					} else {

					}
				}
			}
			$data = array_diff_key($makeArray, $SheetDataKey);
			if (empty($data)) {
				$flag = 1;
			}
			if ($flag == 1) {
				for ($i = 2; $i <= $arrayCount; $i++) {
					$addresses = array();
					$name  = $SheetDataKey['name'];
					$kode  = $SheetDataKey['code'];
					$no_hp = $SheetDataKey['no_hp'];
					$name = filter_var(trim($allDataInSheet[$i][$name]), FILTER_SANITIZE_STRING);
					$kode = filter_var(trim($allDataInSheet[$i][$kode]), FILTER_SANITIZE_STRING);
					$no_hp = filter_var(trim($allDataInSheet[$i][$no_hp]), FILTER_SANITIZE_STRING);
					$fetchData[] = array('code' => $kode, 'name' => $name, 'no_hp' => $no_hp);
				}              
				$data['employeeInfo'] = $fetchData;
				$this->import->setBatchImport($fetchData);
				$this->import->importData('tbl_driver');
			} else {
				echo "Please import correct file";
			}
			redirect('driver');
		}

	}

	public function importFood(){
		

		if ($this->input->post('importbtn')) {
			$path = 'upload/excel/';
			$config['upload_path'] = $path;
			$config['allowed_types'] = 'xlsx|xls|jpg|png';
			$config['remove_spaces'] = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('importfile')) {
				$error = array('error' => $this->upload->display_errors());
			} else {
				$data = array('upload_data' => $this->upload->data());
			}
			if (!empty($data['upload_data']['file_name'])) {
				$import_xls_file = $data['upload_data']['file_name'];
			} else {
				$import_xls_file = 0;
			}
			$inputFileName = $path . $import_xls_file;
			try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
					. '": ' . $e->getMessage());
			}
			$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

			$arrayCount = count($allDataInSheet);
			$flag = 0;
			$createArray = array('code', 'name', 'harga','jumlah');
			$makeArray = array('code' => 'code', 'name' => 'name', 'harga' => 'harga','jumlah'=>'jumlah');
			$SheetDataKey = array();
			foreach ($allDataInSheet as $dataInSheet) {
				foreach ($dataInSheet as $key => $value) {
					if (in_array(trim($value), $createArray)) {
						$value = preg_replace('/\s+/', '', $value);
						$SheetDataKey[trim($value)] = $key;
					} else {

					}
				}
			}
			$data = array_diff_key($makeArray, $SheetDataKey);
			if (empty($data)) {
				$flag = 1;
			}
			if ($flag == 1) {
				for ($i = 2; $i <= $arrayCount; $i++) {
					$addresses = array();
					$kode  = $SheetDataKey['code'];
					$name  = $SheetDataKey['name'];
					$harga = $SheetDataKey['harga'];
					$jumlah = $SheetDataKey['jumlah'];
					$name = filter_var(trim($allDataInSheet[$i][$name]), FILTER_SANITIZE_STRING);
					$kode = filter_var(trim($allDataInSheet[$i][$kode]), FILTER_SANITIZE_STRING);
					$harga = filter_var(trim($allDataInSheet[$i][$harga]), FILTER_SANITIZE_STRING);
					$jumlah = filter_var(trim($allDataInSheet[$i][$jumlah]), FILTER_SANITIZE_STRING);
					$fetchData[] = array('code' => $kode, 'name' => $name, 'harga' => $harga,'jumlah'=>$jumlah);
				}              
				$data['employeeInfo'] = $fetchData;
				$this->import->setBatchImport($fetchData);
				$this->import->importData('tbl_food');
			} else {
				echo "Please import correct file";
			}
			redirect('food');
		}

	}

	public function importPelanggan(){
		

		if ($this->input->post('importbtn')) {
			$path = 'upload/excel/';
			$config['upload_path'] = $path;
			$config['allowed_types'] = 'xlsx|xls|jpg|png';
			$config['remove_spaces'] = TRUE;
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('importfile')) {
				$error = array('error' => $this->upload->display_errors());
			} else {
				$data = array('upload_data' => $this->upload->data());
			}
			if (!empty($data['upload_data']['file_name'])) {
				$import_xls_file = $data['upload_data']['file_name'];
			} else {
				$import_xls_file = 0;
			}
			$inputFileName = $path . $import_xls_file;
			try {
				$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
				$objReader = PHPExcel_IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch (Exception $e) {
				die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
					. '": ' . $e->getMessage());
			}
			$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

			$arrayCount = count($allDataInSheet);
			$flag = 0;
			$createArray = array('code', 'name', 'no_hp');
			$makeArray = array('code' => 'code', 'name' => 'name', 'no_hp' => 'no_hp');
			$SheetDataKey = array();
			foreach ($allDataInSheet as $dataInSheet) {
				foreach ($dataInSheet as $key => $value) {
					if (in_array(trim($value), $createArray)) {
						$value = preg_replace('/\s+/', '', $value);
						$SheetDataKey[trim($value)] = $key;
					} else {

					}
				}
			}
			$data = array_diff_key($makeArray, $SheetDataKey);
			if (empty($data)) {
				$flag = 1;
			}
			if ($flag == 1) {
				for ($i = 2; $i <= $arrayCount; $i++) {
					$addresses = array();
					$name  = $SheetDataKey['name'];
					$kode  = $SheetDataKey['code'];
					$no_hp = $SheetDataKey['no_hp'];
					$name = filter_var(trim($allDataInSheet[$i][$name]), FILTER_SANITIZE_STRING);
					$kode = filter_var(trim($allDataInSheet[$i][$kode]), FILTER_SANITIZE_STRING);
					$no_hp = filter_var(trim($allDataInSheet[$i][$no_hp]), FILTER_SANITIZE_STRING);
					$fetchData[] = array('code' => $kode, 'name' => $name, 'no_hp' => $no_hp);
				}              
				$data['employeeInfo'] = $fetchData;
				$this->import->setBatchImport($fetchData);
				$this->import->importData('tbl_pelanggan');
			} else {
				echo "Please import correct file";
			}
			redirect('pelanggan');
		}

	}
}