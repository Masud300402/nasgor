<?php 
defined("BASEPATH") or exit('NO DIRECT SCRIPT ALLOWED');

class Transaksi extends CI_Controller{

	public function index(){
		cek_session();
		$db['title'] = "penjualan";
		$db['driver'] = $this->db->get('tbl_driver')->result_array();
		$db['pelanggan'] = $this->db->get('tbl_pelanggan')->result_array();
		echo view2('transaksi',$db);
	}

	public function driver(){
		$id  = $this->input->post('driver');
		$query = $this->db->query('SELECT no_hp as hp from tbl_driver where id = "'.$id.'"')->row_array();
		echo json_encode(array('hp'=>$query['hp']));
	}
	public function pelanggan(){
		$id  = $this->input->post('pelanggan');
		$query = $this->db->query('SELECT no_hp as hp from tbl_pelanggan where id = "'.$id.'"')->row_array();
		echo json_encode(array('hp'=>$query['hp']));
	}

	public function kode(){
		$barkode = $this->session->userdata('kode');
		$query = $this->db->query('SELECT code,name,harga_jual,jumlah from tbl_food where code = "'.$barkode.'" ')->result_array();
		$barcode = '';
		$name    = '';
		$harga_jual   = '';
		$stok   = '';
		foreach ($query as $key) {
			$barcode = $key['code'];
			$name = $key['name'];
			$harga_jual = number_format($key['harga_jual']);
			$stok =$key['jumlah'];
		}
		echo json_encode(array('code'=>$barcode,'name'=>$name,'harga'=>$harga_jual,'stok'=>$stok));
	}

	public function getKode2(){
		$barkode = $this->input->post('kode');
		$query = $this->db->query('SELECT code,name,harga_jual,jumlah from tbl_food where code = "'.$barkode.'" ')->result_array();
		$barcode = '';
		$name    = '';
		$harga_jual   = '';
		$stok   = '';
		foreach ($query as $key) {
			$barcode = $key['code'];
			$name = $key['name'];
			$harga_jual = number_format($key['harga_jual']);
			$stok =$key['jumlah'];
		}
		echo json_encode(array('code'=>$barcode,'name'=>$name,'harga'=>$harga_jual,'stok'=>$stok));
	}

	public function addSementara(){
		$this->session->unset_userdata('kode');
		$post = $this->input->post();
		$kode = $post['kode'];
		$sql = $this->db->query('SELECT id,code,harga_jual from tbl_food where code = "'.$kode.'" ')->row_array();
		$this->jumlah     = $post['jumlah'];
		$this->sub_total  = $sql['harga_jual'] * $this->jumlah;
		$this->food_id    =  $sql['id'];
		$this->created_by =  $this->session->userdata('nama');
		$this->db->insert('tbl_transaksi_sementara',$this);
	}

	public function readSementara(){
		$query = $this->db->query('SELECT ts.id,u.name,u.harga_jual,ts.jumlah,ts.sub_total from tbl_food u LEFT JOIN tbl_transaksi_sementara ts on u.id = ts.food_id where ts.created_by = "'.$this->session->userdata('nama').'" ')->result_array();
		$no = 1;
		foreach ($query as $key) {
			?>
			<tr>
				<th><?php echo $no++ ?></th>
				<th><?php echo $key['name'] ?></th>
				<th><?php echo number_format($key['harga_jual']) ?></th>
				<th><?php echo $key['jumlah'] ?></th>
				<th><?php echo number_format($key['sub_total']) ?></th>
				<th><a  class="btn btn-danger btn-sm" onclick="hapus('<?php echo $key['id'];?>')"><b>Delete</b></a></th>
			</tr>
			<?php 
		}

	}

	public function HapusSementara(){
		$url = $this->uri->segment(3);
		$where = array('id'=>$url);
		$this->db->where($where);
		$this->db->delete('tbl_transaksi_sementara');

	}
	public function clear(){
		$this->db->query('DELETE FROM tbl_transaksi_sementara where created_by = "'.$this->session->userdata('nama').'" ');
	}

	public function jmltot(){
		$query = $this->db->query('SELECT SUM(jumlah) as jumlah,SUM(sub_total) as total FROM tbl_transaksi_sementara where created_by = "'.$this->session->userdata('nama').'" 		 ')->result_array();
		$jumlah   = '';

		foreach ($query as $key) {
			$jumlah = $key['jumlah'];
			$total  = number_format($key['total']);
		}
		echo json_encode(array('jumlah'=>$jumlah,'total'=>$total));
	}

	public function simpan(){
		$post = $this->input->post();
		$pesanan = $post['nopesanan'];
		$driver = $post['driver'];
		$tanggal = $post['tanggal'];
		$pelanggan = $post['pelanggan'];
		$keterangan = $post['keterangan'];
		$result = '';
		$bayar = $post['bayar'];
		$select = $this->db->query('SELECT no_pesanan from tbl_transaksi where no_pesanan = "'.$pesanan.'"');
		if ($select->num_rows()>0) {
			$result = 'No Pesanan Sudah Ada';
		}else{
			$result = "Sukses";
			$query = $this->db->query('SELECT food_id,jumlah,sub_total,created_by	 from tbl_transaksi_sementara where created_by = "'.$this->session->userdata('nama').'"')->result_array();
			foreach ($query as $key) {
				$this->db->query('CALL `tbl_transaksi_detail_insert`("'.$key['food_id'].'","'.$pesanan.'","'.$key['jumlah'].'","'.$key['sub_total'].'","'.$key['created_by'].'")');
			}
			$sql = $this->db->query('SELECT no_pesanan,sum(jumlah) as jumlah,sum(sub_total) as total,created_by from tbl_transaksi_detail where no_pesanan = "'.$pesanan.'"')->row_array();
			$kembali = $bayar - $sql['total'] ;
			$this->db->query('CALL `transaksi_insert`("'.$sql['no_pesanan'].'","'.$pelanggan.'","'.$driver.'","'.$tanggal.'","'.$sql['total'].'","'.$sql['jumlah'].'","'.$bayar.'","'.$kembali.'","'.$sql['created_by'].'","'.$keterangan.'")');
			$this->db->query('DELETE FROM tbl_transaksi_sementara where created_by = "'.$this->session->userdata('nama').'" ');
		}
		echo json_encode($result);
		
	}

	public function hapustransaksi(){
		$nop = $this->uri->segment(3);
		$this->db->query('DELETE FROM tbl_transaksi_detail where no_pesanan = "'.$nop.'"');
		$this->db->query('DELETE FROM tbl_transaksi where no_pesanan = "'.$nop.'"');
	}
}