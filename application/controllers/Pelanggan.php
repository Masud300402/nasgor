<?php 
defined("BASEPATH")or exit('NO DIRECT SCRIPT ALLOWED');

class Pelanggan extends CI_Controller{

	public function index(){
		cek_session();
		$this->session->unset_userdata('barkode');
		$db['title'] = "Pelanggan";
		$this->load->helper('form');
		LoadView('pelanggan','index.php',$db);
	}

	public function read(){
		$query = $this->db->get('tbl_pelanggan')->result_array();
		$no = 1;
		foreach ($query as $key) {
			$jumlah = $this->db->query('SELECT sum(a.jumlah) AS jumlah FROM tbl_transaksi a LEFT JOIN tbl_pelanggan p  ON p.id = a.pelanggan_id WHERE p.id = "'.$key['id'].'" ')->row_array();
			$total = $this->db->query('SELECT sum(a.total) AS total FROM tbl_transaksi a LEFT JOIN tbl_pelanggan p  ON p.id = a.pelanggan_id WHERE p.id = "'.$key['id'].'" ')->row_array();
			$jmltransaksi = $this->db->query('SELECT COUNT(*) AS jmltransaksi FROM tbl_transaksi WHERE pelanggan_id = "'.$key['id'].'"')->row_array();
			?>
			<tr>
				<td width="250">
					<div class="btn-group">	
						<button 
						data-id='<?php echo $key['id'] ?>' data-toggle='modal' data-target='#form-edit' type="button" id="edit" class="btn btn-warning btn-sm" ><i class="fa fa-edit"></i>&nbsp&nbspEdit Data</button>
						<button onclick="hapus(<?php echo $key['id'] ?>)" type="button" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i>&nbsp&nbspHapus Data</button>
					</div>
				</td>
				<td><?php echo $no++ ?></td>
				<td><a href="<?php echo base_url('laporan?pelanggan=').$key["code"]?>"><?php echo $key['code'] ?></a></td>
				<td><?php echo $key['name'] ?>		</td>
				<td><?php echo $key['no_hp'] ?></td>
				<td><?php echo $jmltransaksi['jmltransaksi'] ?></td>
				<td><?php echo $jumlah['jumlah'] ?></td>
				<td><?php echo number_format($total['total']) ?></td>
				
			</tr>
			<?php
		}

	}

	public function save(){
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);
		$post = $this->input->post();
		$nama = $this->session->userdata('nama');
		if ($uri3 == 'tambah') {
			$this->name           = $post['nama'];
			$this->no_hp          = $post['hp'];
			$this->code           = $post['kode'];
			$this->created_by     = $nama;
			$this->db->insert('tbl_pelanggan',$this);
			echo kode('tbl_pelanggan','P') ;
		}
		if ($uri3 == 'edit') {
			$this->name           = $post['nama'];
			$this->no_hp          = $post['hp'];
			$this->code           = $post['kode'];
			$this->updated_by     = $nama;
			$this->db->where(array('id' => $uri4 ));
			$this->db->update('tbl_pelanggan',$this);
			echo kode('tbl_pelanggan','P');		
		}
		if ($uri3 == 'hapus') {
			$this->db->where(array('id' => $uri4 ));
			$this->db->delete('tbl_pelanggan');	
			echo kode('tbl_pelanggan','P');
		}
		if ($uri3 == 'load') {
			$sql = $this->db->get_where('tbl_pelanggan',array('id'=>$uri4))->result_array();

			foreach ($sql as $key) {
				$name           = $key['name'];
				$id             = $key['id'];
				$no_hp          = $key['no_hp'];
				$code           = $key['code'];		
			}
			echo json_encode(array('id'=>$id,'nama'=>$name,'no_hp'=>$no_hp,'kode'=>$code));
		}

		
	}
}