<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>	
			<li>
				<a href="<?php echo base_url('Dashboard') ?>">
					<i class="fa fa-angle-double-left fa-lg"></i>
					 <span>Kembali</span>
				</a>
			</li>
			<li>
				<a href="<?php echo base_url('Login/Logout') ?>">
					<i class="fa fa-angle-double-left fa-lg"></i>
					 <span>Log Out</span>
				</a>
			</li>
		</ul>
	</section>
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


