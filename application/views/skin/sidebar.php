<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>	
			<?php 
			$role = $this->session->userdata('role');
			$sqlmenu = $this->db->query("SELECT menu.*
				FROM tbl_menu_role
				 JOIN tbl_menu menu ON menu.id = tbl_menu_role.menu_id AND tbl_menu_role.group_users_id = ".$role." order by menu.id asc ");
			$queryMenu = $sqlmenu->result_array();	
			foreach ($queryMenu as $key) {	
				?>
				<?php if ($key['menu_parent'] == 'No' && $key['menu_parent_id'] == '0'): ?>
					<li class="<?php echo ($title==$key['menu_desc'])?'active ':'';?>">
						<a href="<?php echo base_url($key['menu_url']) ?>">
							<i class="fa fa-lg fa-<?php echo $key['menu_icon']?>"></i> <span><?php echo $key['menu_name'] ?></span>
						</a>
					</li>
				<?php endif ?>
				<?php if ($key['menu_parent'] == 'Yes'): ?>
					<li class="treeview">
						<a >
							<i class="fa fa-lg fa-<?php echo $key['menu_icon']?>"></i> <span><?php echo $key['menu_name'] ?></span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="  treeview-menu ">
							<?php 
							$sqlparent = $this->db->query("SELECT menu.*
								FROM tbl_menu_role
								JOIN tbl_menu menu ON menu.id = tbl_menu_role.menu_id AND tbl_menu_role.group_users_id = ".$role." AND menu.menu_parent_id = '".$key['id']."'   ");
							$queryparent = $sqlparent->result_array();

							foreach ($queryparent as $parent) {


								?>

								<li class="<?php echo ($title==$parent['menu_desc'])?'active ':'';?>">
									<a href="<?php echo $parent['menu_url'] ?>"><i class="fa fa-lg fa-<?php echo $parent['menu_icon']?>"></i><?php echo $parent['menu_name'] ?></a>
								</li>

							<?php } ?>
						</ul>
					</li>	
				<?php endif ?>
			<?php } ?>
		</ul>
	</section>
</aside>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


