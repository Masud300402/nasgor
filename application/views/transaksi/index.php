
<div class="container-fluid" >
<!-- Main content -->
<section class="content">
<div class="row">
	<div class="col-md-12">
		<div class="box box-success box-solid ">
			<div class="box-body">

				<div class="box box-info box-solid">

					<div class="table-responsive">
						<table style="margin-top: 2%;" class="table table-bordered table-striped ">
							<tr>
								<th class="bg-teal" colspan="6" >Data penjualan</th>
							</tr>
							<tr>
								<th class="bg-default">No Pesanan  </th>
								<th>:</th>
								<th>
									<input type="text" name="pesanan" id="pesanan" class="form-control"></th>
									<th class="bg-default">Tanggal  </th>
									<th>:</th>
									<th>
										<input type="text" name="tanggal" id="tanggal" class="form-control" autocomplete="off"></th>



									</tr>

									<tr>
										<th class="bg-default">Driver  </th>
										<th>:</th>
										<th>
											<select id="driver" name="driver" style="width: 100%" >
												<option value="">Pilih Driver</option>
												<?php foreach ($driver as $dri): ?>
													<option  value="<?php echo $dri['id'] ?>" ><?php echo $dri['name'] ?></option>
												<?php endforeach ?>
											</select>
										</th>
										<th class="bg-default">Hp Driver  </th>
										<th>:</th>
										<th>
											<input type="text" readonly name="hpdriver" id="hpdriver" class="form-control"></th>
										</tr>

										<tr>
											<th class="bg-default">Pelanggan  </th>
											<th>:</th>
											<th>
												<select id="pelanggan" name="pelanggan"  style="width: 100%"   >
													<option value="">Pilih Pelanggan</option>

													<?php foreach ($pelanggan as $pb): ?>
														<option value="<?php echo $pb['id'] ?>">
															<?php echo $pb['name'] ?>

														</option>
													<?php endforeach ?>
												</select>
												<th class="bg-default">Hp pelanggan  </th>
												<th>:</th>
												<th>
													<input type="text" readonly name="hppembeli" id="hppembeli" class="form-control"></th>
												</tr>
												<tr>
													<th class="bg-default">Keterangan</th>
													<th>:</th>
													<th colspan="4">
														<textarea name="keterangan" class="form-control" id="keterangan"></textarea>
													</th>	
												</tr>
											</table>
										</div>
									</div>

									<div class="box box-info box-solid">	
										<div class="table-responsive " >
											<table style="margin-top: 2%;" class="table table-bordered table-striped" >
												<tr>
													<th class="bg-teal" colspan="10" >Input Barang</th>

												</tr>
												<tr>
													<th class="bg-default"  >Kode  </th>
													<th>:</th>
													<th class="bg-default" >
														<div class="form-inline" >

															<input id="kode" type="text" class="form-control" name="kode" style="width: 83%">
															<a href="cari_makanan?menu=transaksi" class="btn btn-info "><i class="fa fa-search"></i></a>

														</div>
													</th>

													<th class="bg-default" >Nama Makanan </th>
													<th>:</th>
													<th class="bg-default"  >
														<input readonly id="makanan" type="text" class="form-control" name="makanan">
													</th>

													<th class="bg-default" >Saldo </th>
													<th>:</th>
													<th class="bg-default"  >
														<input readonly id="stok" type="text" class="form-control" name="stok">
													</th>

												</tr>
												<tr>
													<th class="bg-default"  >Harga Jual(Rp) </th>
													<th>:</th>
													<th class="bg-default"  >

														<input id="harga" readonly type="text"  class="form-control" name="harga"> 


													</th>

													<th>Jumlah</th>
													<th>:</th>
													<th>	
														<input id="jumlah" type="number"  class="form-control" name="">
													</th>
													<th><button class="btn" id="add">Tambah</button></th>
													<th></th>
													<th></th>
												</tr>


											</table>
										</div>
									</div>

									<div class="box box-info box-solid">	
										<h4><b>Daftar Barang</b></h4>
										<div class="table-responsive table-bordered table-hover table-striped">
											<table class="table">
												<thead>
													<tr class="bg-teal" >
														<th>No</th>
														<th>Nama Makanan</th>
														<th>Harga</th>
														<th>Jumlah</th>
														<th>Sub Total(RP)</th>
														<th width="20">
															<button id="clear" class="btn btn-block btn-danger">Clear</button>
														</th>
													</tr>
												</thead>
												<tbody id="data">

												</tbody>
												<tfoot class="bg-default">
													<tr>
														<th colspan="3"></th>
														<th>Grand Total Belanja(Rp.) :</th>
														<th>
															<input type="text" class="btn" id="jml" name="jml" readonly>
														</th>
														<th>
															<input type="text" class="btn" id="total" name="total" readonly>
														</th>
														<th></th>

													</tr>
													<tr>
														<th colspan="3"></th>
														<th>Uang Bayar(Rp.) :</th>
														<th></th>
														<th><input type="text" class="form-control" name="bayar" id="bayar" autocomplete="off" ></th>
														<th></th>
													</tr>
													<tr>
														<th colspan="3"></th>
														<th>Uang Kembali(Rp.) :</th>
														<th></th>
														<th><input type="text" class="form-control" name="bayar" id="kembali" readonly></th>
														<th></th>
													</tr>
													<tr>
														<th colspan="5"></th>
														<th><a ><button type="button" id="simpan" class="btn btn-warning btn-block">Simpan pembayaran</button></a></th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
								<!-- /.box-body -->
							</div>
							<!-- /.box -->
						</div>
						<!-- /.col -->
					</div>
					<!-- /.row -->
				</section>
			</div>

			<script type="text/javascript" >
				function addCommas(nStr)
					{
						nStr += '';
						x = nStr.split('.');
						x1 = x[0];
						x2 = x.length > 1 ? '.' + x[1] : '';
						var rgx = /(\d+)(\d{3})/;
						while (rgx.test(x1)) {
							x1 = x1.replace(rgx, '$1' + ',' + '$2');
						}
						return x1 + x2;
					}
				$(document).ready(function(){

					

					$('#bayar').keyup(function(){

						var bayar = $('#bayar').val().split(',').join('');
						var total =  $('#total').val().split(',').join('');
						var kembali = bayar - total;
						$('#kembali').val(addCommas(kembali));
					})
					$("#bayar").inputmask({
						'alias': 'decimal',
						'rightAlign' : true,
						'groupSeparator': '.',
						'autoGroup': true
					});
					$('#tanggal').datepicker({
						autoclose: true,
						format: 'yyyy-mm-dd',
					})

					$('#jumlah').val('1');
					$('#pesanan').val(sessionStorage.getItem('nopesanan'));
					$('#keterangan').val(sessionStorage.getItem('keterangan'));

					$('#driver option').each(function(){
						var i = $(this).val();
						if ($(this).val() == sessionStorage.getItem('driver')) {
							$(this).attr('selected','selected')
							$.ajax({
								url:"<?php echo base_url('transaksi/driver') ?>",
								data:{driver:i},
								dataType:"JSON",
								type:"POST",
								success:function(data){
									$('#hpdriver').val(data.hp);
								}
							})
						}
					})

					$('#pelanggan option').each(function(){
						var a = $(this).val();
						if ($(this).val() == sessionStorage.getItem('pelanggan')) {
							$(this).attr('selected','selected')

							$.ajax({
								url:"<?php echo base_url('transaksi/pelanggan') ?>",
								data:{pelanggan:a},
								dataType:"JSON",
								type:"POST",
								success:function(data){
									$('#hppembeli').val(data.hp);
								}
							})
						}
					})

					if (sessionStorage.getItem('tanggal') == '' || !sessionStorage.getItem('tanggal') ) {
						$('#tanggal').datepicker('setDate', new Date());

					}else{
						$('#tanggal').val(sessionStorage.getItem('tanggal'));

					}



					$('#pesanan').keyup(function(){
						sessionStorage.setItem('nopesanan',$(this).val());
					})
					$('#keterangan').keyup(function(){
						sessionStorage.setItem('keterangan',$(this).val());
					})
					$('#tanggal').change(function(){
						sessionStorage.setItem('tanggal',$(this).val());
					})
					$('#driver').change(function(){
						sessionStorage.setItem('driver',$(this).val());
					})
					$('#pelanggan').change(function(){
						sessionStorage.setItem('pelanggan',$(this).val());
					})
					$('#driver').change(function(){

						var i = $(this).val();
						$.ajax({
							url:"<?php echo base_url('transaksi/driver') ?>",
							data:{driver:i},
							dataType:"JSON",
							type:"POST",
							success:function(data){
								$('#hpdriver').val(data.hp);
							}
						})
					})
					$('#pelanggan').change(function(){

						var a = $(this).val();
						$.ajax({
							url:"<?php echo base_url('transaksi/pelanggan') ?>",
							data:{pelanggan:a},
							dataType:"JSON",
							type:"POST",
							success:function(data){
								$('#hppembeli').val(data.hp);
							}
						})
					})


					$('select').select2();
				})
				kode()
				addSementara()
				readSementara()
				clear()
				jmltot()
				simpan()
				changekode()
				function kode(){
					$.ajax({
						url:"<?php echo base_url('transaksi/kode') ?>",
						dataType:"JSON",
						type:"POST",
						success:function(data){
							$('#kode').val(data.code);
							$('#makanan').val(data.name);
							$('#harga').val(data.harga);
							$('#stok').val(data.stok);
						}
					})
				}

				function addSementara(){
					$('#add').click(function(){
						var kode = $('#kode').val();
						var jumlah = $('#jumlah').val();

						$.ajax({
							url:"<?php echo base_url('transaksi/addSementara') ?>" ,
							data:{kode:kode,jumlah:jumlah},
							type:'POST',
							success:function(){
								$('#kode').val('');
								$('#makanan').val('');
								$('#harga').val('');
								$('#jumlah').val('0');
								$('#stok').val('');
								readSementara();
								jmltot();
							}

						})
					})
				}

				function readSementara(){
					$.ajax({
						url:'<?php echo base_url('transaksi/readSementara') ?>',
						type:"POST",
						success:function(data){
							$('#data').html(data);
						}
					})
				}

				function hapus(id){
					$.ajax({
						url:"<?php echo base_url('transaksi/HapusSementara/') ?>/" +id,
						type:'POST',
						cache: false,
						contentType: false,
						processData: false,
						success:function(){
							readSementara();
							jmltot();

						}
					})
				}

				function changekode() {
					$('#kode').change(function(){
						var code = $(this).val();
						$.ajax({
							url:site_url+'transaksi/getKode2',
							type:'POST',
							data:{kode:code},
							dataType:'JSON',
							success:function(data){
								$('#kode').val(data.code);
								$('#makanan').val(data.name);
								$('#harga').val(data.harga);
								$('#stok').val(data.stok);
							}
						})
					})
				}

				function clear(){
					$('#clear').click(function(){
						$.ajax({
							url:site_url + 'transaksi/clear',
							type:"POST",
							success:function(){
								readSementara();
								jmltot()

							}
						})
					})
				}

				function jmltot(){
					$.ajax({
						url:site_url + 'transaksi/jmltot',
						type:"POST",
						dataType:"JSON",
						success:function(data){
							$('#jml').val(data.jumlah);
							$('#total').val(data.total);
						}
					})
				}

				function simpan(){
					$('#simpan').click(function(){
						var nopesanan = $('#pesanan').val();
						var driver    = $('#driver').val();
						var pelanggan = $('#pelanggan').val();
						var tanggal   = $('#tanggal').val();
						var i         = $('#bayar').val();
						var keterangan= $('#keterangan').val();
						var bayar     = i.split(',').join('');

						$.ajax({
							url:"<?php 	echo base_url('transaksi/simpan') ?>",
							type:"POST",
							data:{nopesanan:nopesanan,driver:driver,pelanggan:pelanggan,bayar:bayar,tanggal:tanggal,keterangan:keterangan},
							dataType:"JSON",
							success:function(data){
								readSementara();
								jmltot()
								$('#bayar').val('');
								$('#kembali').val('');
								alert(data);
							}
						})
					})
				}
			</script>