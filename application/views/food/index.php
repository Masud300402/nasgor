<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info box-solid">
        <div class="box-header">
          <h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Data Makanan </b></h2>

          <div class="box-tools">
            <div class="margin">

             <a  class="btn btn-warning"  href="<?php echo base_url(); ?>upload/format/format_import_data_food.xlsx">
              <span data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left"  data-original-title="Download Format Import Data "><i class="fa fa-file-excel-o"></i>&nbsp&nbspFormat Import Data
              </span>
            </a>

            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#form-import">
              <span data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left"  data-original-title="Import Data "><i class="fa fa-download"></i>&nbspImport Data
              </span>

            </button>
             <button type="button" class="btn btn-success" data-toggle="modal" data-target="#form-add">
              <span data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left"  data-original-title="Tambah Data ">
                <i class="fa fa-plus"></i>&nbspTambah Data
              </span>
            </button>

          </div>
        </div>

      </div>

      <!-- /.box-header -->
      <div class="box-body">
        <div class="table table-responsive">  
          <table id="tabel" class="table table-bordered table-hover">
            <thead >
              <tr class="bg-info">
                <th>Opsi</th>
                <th>No</th>
                <th>Kode</th>
                <th>Nama</th>
                <th>Harga Beli</th>
                <th>Harga Jual</th>
                <th>Jumlah</th>

              </tr>
            </thead>
            <tbody id="data">


            </tbody>
          </table>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<div class="modal fade" id="form-add">
  <div class="modal-dialog modal-md ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Data</h4>
        </div>
        <div class="modal-body modal-add">

          <form class="form-horizontal" id="form-tambah" method="post" enctype="multipart/form-data">


            <div class="form-group">
              <label for="nama" class="col-sm-2 control-label">Kode  </label>

              <div class="col-sm-10">
                <input type="text" name="kode" class="form-control" id="kode" required="">
              </div>

            </div>

            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Nama Makanan</label>

              <div class="col-sm-10">
                <input type="text" name="nama"  class="form-control" id="nama" >
              </div>

            </div>
            
            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Harga beli</label>

              <div class="col-sm-10">
                <input type="text" name="hargabeli"  class="form-control" id="hargabeli" >
              </div>

            </div>

            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Harga Jual</label>

              <div class="col-sm-10">
                <input type="text" name="hargajual"  class="form-control" id="hargajual" >
              </div>

            </div>

            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Jumlah</label>

              <div class="col-sm-10">
                <input type="text" name="jumlah"  class="form-control" id="jumlah" >
              </div>

            </div>
            <div class="modal-footer">
              <button type="reset" class="btn btn-warning pull-left">Reset</button>
              <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"  >Save changes</button>
            </div>
          </div>

        </form>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  </div>
</div>



<div class="modal fade" id="form-edit">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body modal-edit">
          <font color="red" class="text-center"><div class="error"></div></font>
          <form class="form-horizontal" id="form-update" method="post" enctype="multipart/form-data">

            <input type="hidden" name="id" id="id">
            <div class="form-group">
              <label for="nama" class="col-sm-2 control-label">Kode  </label>

              <div class="col-sm-10">
                <input type="text" name="kode" class="form-control" id="kode" required="">
              </div>

            </div>

            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Nama Makanan</label>

              <div class="col-sm-10">
                <input type="text" name="nama"  class="form-control" id="nama" >
              </div>

            </div>
            
            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Harga beli</label>

              <div class="col-sm-10">
                <input type="text" name="hargabeli"  class="form-control" id="hargabeli" >
              </div>

            </div>

            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Harga Jual</label>

              <div class="col-sm-10">
                <input type="text" name="hargajual"  class="form-control" id="hargajual" >
              </div>

            </div>

            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Jumlah</label>

              <div class="col-sm-10">
                <input type="text" name="jumlah"  class="form-control" id="jumlah" >
              </div>

            </div>
            <div class="modal-footer">
              <button type="reset" class="btn btn-warning pull-left">Reset</button>
              <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"  >Save changes</button>
            </div>
            <!-- /.modal-content -->
          </div>
        </form>

      </div>
    </div>
  </div>
</div>

<!-- /.modal-dialog -->

<div class="modal fade" id="form-import">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"></div>
      <div class="modal-body  ">
        <?php
        $output = '';
        $output .= form_open_multipart('import/importFood');
        $output .= '<div class="row">';
        $output .= '<div class="col-lg-12 col-sm-12"><div class="form-group">';
        $output .= form_label('Import Data', 'image');
        $data = array(
          'name' => 'importfile',
          'id' => 'importfile',
          'class' => 'form-control filestyle',
          'value' => '',
          'data-icon' => 'false'
        );
        $output .= form_upload($data);
        $output .= '</div> <span style="color:red;">*Please choose an Excel file(.xls or .xlxs) as Input</span></div>';
        $output .= '<div class="col-lg-12 col-sm-12"><div class="form-group text-right">';
        $data = array(
          'name' => 'importbtn',
          'id' => 'importbtn',
          'class' => 'btn btn-primary',
          'value' => 'Import',
        );
        $output .= form_submit($data, 'Import Data');
        $output .= '</div>
        </div></div>';
        $output .= form_close();
        echo $output;
        ?>
      </div>
      <div class="modal-footer"></div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){

    $(".modal-add #hargajual").inputmask({
      'alias': 'decimal',
      'rightAlign' : true,
      'groupSeparator': '.',
      'autoGroup': true
    });

    $(".modal-add #hargabeli").inputmask({
      'alias': 'decimal',
      'rightAlign' : true,
      'groupSeparator': '.',
      'autoGroup': true
    });

    $(".modal-edit #hargajual").inputmask({
      'alias': 'decimal',
      'rightAlign' : true,
      'groupSeparator': '.',
      'autoGroup': true
    });

    $(".modal-edit #hargabeli").inputmask({
      'alias': 'decimal',
      'rightAlign' : true,
      'groupSeparator': '.',
      'autoGroup': true
    });

    $('#form-add').submit(function(e){
      e.preventDefault();
      var data = $('#form-tambah').serialize();
      $.ajax({
        url: base_url + 'food/save/tambah/',
        type:"POST",
        data:data,
        success:function(i){
          $('#form-add').modal('hide');
          read();
          document.getElementById('form-tambah').reset()
          $('#kode').val(i);
        }
      })
    })


    $(document).on('click','#edit',function(){
      var id   = $(this).data('id');

      $.ajax({
        url:base_url +'food/save/load/' + id,
        type:"POST",
        dataType:"JSON",
        success:function(data){
          $('.modal-edit #id').val(data.id);
          $('.modal-edit #hargajual').val(data.hargajual);
          $('.modal-edit #hargabeli').val(data.hargabeli);
          $('.modal-edit #nama').val(data.nama);
          $('.modal-edit #jumlah').val(data.jumlah);
          $('.modal-edit #kode').val(data.code);
        }
      })

    })

    $('#form-update').submit(function(e){
      e.preventDefault();
      var data = $('#form-update').serialize();
      var id = $('#id').val();
      $.ajax({
        url: site_url + 'food/save/edit/'+id,
        type:"POST",
        data:data,
        success:function(i){
          read();
          $('#form-edit').modal('hide');
          $('#kode').val(i);
        }
      })     
    })



    $(document).ready(function(){
      $('#page').on('change', function(e){
        e.preventDefault();
        var newSize = $(this).val();
        FooTable.get('.table').pageSize(newSize);
      });
    })
  })

  read()

  function read(){
    $.ajax({
      url:base_url + 'food/Read',
      type:'POST',
      success:function(data){
        $('tbody').html(data);
        $('.table').footable({
          "paging": {
            "enabled": true
          },
          "filtering": {
            "enabled": true
          },
          "sorting": {
            "enabled": true
          }
        });
      }
    })
  }

  function hapus(id){
   swal({
    title: "Are you sure?",
    text: "You will not be able to recover this imaginary file!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it",
    cancelButtonText: "No, cancel",
    closeOnConfirm: false,
    closeOnCancel: false
  }, function (isConfirm) {
    if (isConfirm) {
      $.ajax({
        url : base_url + 'food/save/hapus/'+id,
        type : 'POST',
        cache: false,
        contentType: false,
        processData: false,
        success:function(i){

          swal({
            title: "Deleted!",
            text: "Your imaginary file has been deleted.",
            type: "success",
          }, function (isConfirm) {
            read();
            $('#kode').val(i);
          });

        }
      });
    } else {
      swal("Cancelled", "Your imaginary file is safe :)", "error");
    }
  });
 }
</script>