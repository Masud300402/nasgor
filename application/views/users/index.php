<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info box-solid">
        <div class="box-header">
          <h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Data Users </b></h2>

          <div class="box-tools">
            <div class="margin">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#form-add">
                <span data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left"  data-original-title="Tambah Data "><i class="fa fa-plus"></i>
                </span>
              </button>

            </div>
          </div>

        </div>

        <!-- /.box-header -->
        <div class="box-body">
          <div class="table table-responsive">  
            <table id="tabel" class="table table-bordered table-hover">
              <thead >
                <tr class="bg-info">
                  <th>Opsi</th>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Username</th>
                  <th>Role</th>

                </tr>
              </thead>
              <tbody id="data">


              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<div class="modal fade" id="form-add">
  <div class="modal-dialog modal-md ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Data</h4>
        </div>
        <div class="modal-body">
          <font color="red" class="text-center"><div class="error"></div></font>
          <form class="form-horizontal" id="form-tambah" method="post" enctype="multipart/form-data">


            <div class="form-group">
              <label for="nama" class="col-sm-2 control-label">Nama  </label>

              <div class="col-sm-10">
                <input type="text" name="nama" class="form-control" id="name" required="">
              </div>

            </div>

            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Username</label>

              <div class="col-sm-10">
                <input type="text" name="username"  class="form-control" id="username" >
              </div>

            </div>

            <div class="form-group">
              <label for="password" class="col-sm-2 control-label">Password</label>

              <div class="col-sm-10">
                <input type="password"  name="password" class="form-control" id="password" >
              </div>

            </div>

            <div class="form-group">
              <label for="password" class="col-sm-2 control-label">Re Password</label>

              <div class="col-sm-10">
                <input type="password" name="repass" class="form-control" id="repass" required="">
              </div>

            </div>

            <div class="form-group">
              <label for="group" class="col-sm-2 control-label">Role</label>

              <div class="col-sm-10">
                <select id="group" name="group" class="form-control">
                  <?php foreach ($group as $key): ?>
                    <option value="<?php echo $key['id'] ?>"><?php echo $key['name'] ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>



            <div class="modal-footer">
              <button type="reset" class="btn btn-warning pull-left">Reset</button>
              <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary"  >Save changes</button>
            </div>
          </div>

        </form>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
  </div>
</div>



<div class="modal fade" id="form-edit">
  <div class="modal-dialog ">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Data</h4>
        </div>
        <div class="modal-body modal-edit">
          <font color="red" class="text-center"><div class="error"></div></font>
          <form class="form-horizontal" id="form-update" method="post" enctype="multipart/form-data">

            <input type="hidden" name="id" id="id">
            <div class="form-group">
              <label for="nama" class="col-sm-2 control-label">Nama  </label>

              <div class="col-sm-10">
                <input type="text" name="nama" class="form-control" id="nama" required="">
              </div>

            </div>

            <div class="form-group">
              <label for="username" class="col-sm-2 control-label">Username</label>

              <div class="col-sm-10">
                <input type="text" name="username"  class="form-control" id="username" >
              </div>

            </div>

            <div class="form-group">
              <label for="password" class="col-sm-2 control-label">Password</label>

              <div class="col-sm-10">
                <input type="password"  name="password" class="form-control" id="password" >
              </div>

            </div>

            <div class="form-group">
              <label for="password" class="col-sm-2 control-label">Re Password</label>

              <div class="col-sm-10">
                <input type="password" name="repass" class="form-control" id="repass" required="">
              </div>

            </div>

            <div class="form-group">
              <label for="group" class="col-sm-2 control-label">Role</label>

              <div class="col-sm-10">
                <select id="group" name="group" class="form-control">
                  <?php foreach ($group as $key): ?>
                    <option value="<?php echo $key['id'] ?>"><?php echo $key['name'] ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            </div>



            <div class="modal-footer">
              <button type="reset" class="btn btn-warning pull-left">Reset</button>
              <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </div>

        </form>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <script type="text/javascript">
      $(document).ready(function(){

        $('#form-add').submit(function(e){
          e.preventDefault();

          if ($('#password').val() === $('#repass').val()) {

            var data = $('#form-tambah').serialize();
            console.log(data);

            $.ajax({
              url: base_url + 'users/save/tambah/',
              type:"POST",
              data:data,
              success:function(){
                $('#form-add').modal('hide');
                read();
                document.getElementById('form-tambah').reset()
              }
            })
          }else{
            alert('Password Dan Repassword Tidak Sama');
          }

        })


        $(document).on('click','#edit',function(){
          var id   = $(this).data('id');

          $.ajax({
            url:base_url +'users/save/load/' + id,
            type:"POST",
            dataType:"JSON",
            success:function(data){
              $('.modal-edit #id').val(data.id);
              $('.modal-edit #nama').val(data.nama);
              $('.modal-edit #username').val(data.username);
              $('.modal-edit #group option').each(function(){
               if ($(this).text() == data.role){
                $(this).attr("selected","selected");
              }
            })
            }
          })

        })

        $('#form-update').submit(function(e){
          e.preventDefault();

          if ($('.modal-edit #password').val() === $('.modal-edit #repass').val()) {
            var data = $('#form-update').serialize();
            var id = $('#id').val();
            $.ajax({
              url: site_url + 'users/save/edit/'+id,
              type:"POST",
              data:data,
              success:function(){
                read();
                $('#form-edit').modal('hide');
              }
            })
          }else{
            alert('Password Dan Repassword Tidak Sama');
          }
        })



        $(document).ready(function(){
          $('#page').on('change', function(e){
            e.preventDefault();
            var newSize = $(this).val();
            FooTable.get('.table').pageSize(newSize);
          });
        })
      })

      read()

      function read(){
        $.ajax({
          url:base_url + 'users/Read',
          type:'POST',
          success:function(data){
            $('tbody').html(data);
            $('.table').footable({
              "paging": {
                "enabled": true
              },
              "filtering": {
                "enabled": true
              },
              "sorting": {
                "enabled": true
              }
            });
          }
        })
      }

      function hapus(id){
       swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it",
        cancelButtonText: "No, cancel",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            url : base_url + 'users/save/hapus/'+id,
            type : 'POST',
            cache: false,
            contentType: false,
            processData: false,
            success:function(){

              swal({
                title: "Deleted!",
                text: "Your imaginary file has been deleted.",
                type: "success",
              }, function (isConfirm) {
                read();
              });

            }
          });
        } else {
          swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
      });
     }
   </script>