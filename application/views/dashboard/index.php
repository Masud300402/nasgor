<section class="content">
  <div class="row">
    <div class="col-md-6 col-sm-3 ">
      <div class="info-box bg-yellow">
        <span class="info-box-icon"><i class="fa fa-cart-arrow-down"></i></span>
        <div class="info-box-content">
          <span class="info-box-text"><b>Transaksi Hari Ini</b></span>
          <span class="info-box-number"><?php echo number_format($Today['today']) ?>&nbsp Barang </span>

          <div class="progress">
            <div class="progress-bar" style="width: 100%"></div>
          </div>

          <span class="progress-description" >
           <a href="<?php echo base_url('laporan?tanggal=').date('Y-m-d') ?>"><font color="white" ><b style="cursor: pointer">Lihat Barang</b></font></a>
         </span>
       </div>
       <!-- /.info-box-content -->
     </div>
     <!-- /.info-box -->
   </div>

   <div class="col-md-6 col-sm-3 ">
    <div class="info-box bg-teal">
      <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

      <div class="info-box-content">
        <span class="info-box-text"><b>Nominal Transaksi Hari Ini</b></span>
        <span class="info-box-number">Rp.<?php echo number_format($nToday['ntoday'])?></span>

        <div class="progress">
          <div class="progress-bar" style="width: 100%"></div>
        </div>

        <span class="progress-description" >
         <a href="<?php echo base_url('laporan?tanggal=').date('Y-m-d') ?>"><font color="white" ><b style="cursor: pointer">Lihat Barang</b></font></a>       </span>
       </div>
       <!-- /.info-box-content -->
     </div>
     <!-- /.info-box -->
   </div>

   <div class="col-sm-12">
    <div class="box box-info box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Nominal Transaksi Chart Bulanan</h3>


      </div>
      <div class="box-body">
        <div class="chart">
          <canvas id="myChartMonth" style="height:230px"></canvas>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>

  <div class="col-sm-12">
    <div class="box box-info box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Nominal Transaksi Chart Tahunan</h3>


      </div>
      <div class="box-body">
        <div class="chart">
          <canvas id="myChart" style="height:230px"></canvas>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>

</div>
</section>
<script src="<?=base_url('assets/plugins/chart.js/Chart.min.js')?>"></script>
<script>
  $(document).ready(function(){
  
    $('#tahun').datepicker({
     format: 'yyyy',
     minViewMode: 'years',
   })

    $('#tahun').datepicker('setDate', new Date());

    showGraphYears()
    showGraphMonth()


    
  })

    function addCommas(nStr)
    {
      nStr += '';
      x = nStr.split('.');
      x1 = x[0];
      x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
    }

    function showGraphMonth(){
      var ctx = document.getElementById('myChartMonth').getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ['1', '2', '3', '4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'],
          datasets: [{
            label: 'Transaksi',
            data: [<?php echo chart3()?>],
            backgroundColor: 'rgba(153, 102, 255, 0.2)',
            borderColor: 'rgba(75, 192, 192, 1)',
            borderWidth: 3
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true,
                userCallback: function(value, index, values) {
                  value = value.toString();
                  value = value.split(/(?=(?:...)*$)/);
                  value = value.join('.');
                  return 'Rp.' + value;
                }
              }
            }]
          },
          tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
              label: function(tooltipItem, data) {
                return "Rp." + addCommas(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);

              },

            }
          }
        }
      });
    }
    function showGraphYears(){


      var ctx = document.getElementById('myChart').getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ['January', 'February', 'Maret', 'April', 'Mei', 'Juni','July','Agustus','September','November','Desember'],
          datasets: [{
            label: 'Transaksi',
            data: [<?php echo chart2()?>],
            backgroundColor: 'rgba(153, 102, 255, 0.2)',
            borderColor: 'rgba(75, 192, 192, 1)',
            borderWidth: 3
          }]
        },
        options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero:true,
                userCallback: function(value, index, values) {
                  value = value.toString();
                  value = value.split(/(?=(?:...)*$)/);
                  value = value.join('.');
                  return 'Rp.' + value;
                }
              }
            }]
          },
          tooltips: {
            enabled: true,
            mode: 'single',
            callbacks: {
              label: function(tooltipItem, data) {
                return "Rp." + addCommas(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);

              },

            }
          }
        }
      });
    }


  </script>
