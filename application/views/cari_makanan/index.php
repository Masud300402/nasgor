	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info box-solid">
					<div class="box-header">
						<div class="row">
							<div class="col-md-12">
								<div class="box-title">
									<b><h3>Pencarian Barang</h3></b>
								</div>
							</div>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table style="margin-top: 2%" class="table">
							<thead>
								<tr  class="bg-gray">
									<th>No</th>
									<th>Kode</th>
									<th>Nama Makanan</th>
									<th>Jumlah</th>
									<th>Hrg Jual(Rp)</th>
								</tr>
							</thead>
							<tbody id="tbody">
								<?php $no=1; foreach ($barang as $key): ?>
								<tr>
									<th><?php echo $no++ ?></th>
									<th><a id="kode"><?php echo $key['code'] ?></a></th>
									<th><?php echo $key['name'] ?></th>
									<th><?php echo $key['jumlah'] ?></th>
									<th><?php echo $key['harga_jual'] ?></th>

								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	<!-- /.row -->
</section>
<script type="text/javascript">

	$(document).ready(function(){

		$('.table').DataTable();
		$(document).on('click','#kode',function(){
			var data = $(this).text()

			$.ajax({
				url:"<?php echo site_url('cari_makanan/kode') ?>",
				type:"POST",
				data:{data:data},
				success:function(sukses){
					window.location.href = "<?php echo site_url($_GET['menu']) ?>"
				}
			})

		})


	})


</script>