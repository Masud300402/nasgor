<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info box-solid">
        <div class="box-header">
          <h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Data Transaksi </b></h2>
          
          <div class="box-tools">

            <button type="button" class="btn btn-danger" id="hapus" >
              <span data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left"  data-original-title="Delete Data "><i class="fa fa-trash"></i>&nbspDelete Data
              </span>
            </button>



          </div>

        </div>

        <!-- /.box-header -->
        <div class="box-body">
          <div class="table table-responsive">  
            <table class="table">
              <thead>
                <tr class="bg-gray" >
                  <th>No</th>
                  <th>No Pesanan</th>
                  <th>Nama Makanan</th>
                  <th>Harga</th>
                  <th>Jumlah</th>
                  <th>Sub Total(RP)</th>
                  <th></th>
                </tr>
              </thead>
              <tbody >
                <?php $i=1; foreach ($query as $key ): ?>
                <tr>  
                  <td> <?php echo $i++ ?></td>
                  <td> <?php echo $key['no_pesanan'] ?></td>
                  <td> <?php echo $key['nama'] ?></td>
                  <td> <?php echo number_format($key['harga']) ?></td>
                  <td> <?php echo $key['jumlah'] ?></td>
                  <td> <?php echo number_format($key['sub_total']) ?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
            <tfoot class="bg-default">
              <tr>
                <th colspan="3"></th>
                <th>Grand Total Belanja(Rp.) :</th>
                <th>
                  <input type="text"  id="jml" name="jml" 
                  value="<?php echo $query2['jumlah'] ?>" class='text-center form-control' readonly>
                </th>
                <th>
                  <input type="text"  id="total" name="total"
                  value="<?php echo number_format($query2['total']) ?>" class='text-center form-control' readonly>
                </th>
                <th></th>

              </tr>
              <tr>
                <th colspan="3"></th>
                <th>Uang Bayar(Rp.) :</th>
                <th></th>
                <th><input type="text" 
                  value="<?php echo number_format($query2['uang_bayar']) ?>" class="form-control text-center" name="bayar" id="bayar" readonly></th>
                  <th></th>
                </tr>
                <tr>
                  <th colspan="3"></th>
                  <th>Uang Kembali(Rp.) :</th>
                  <th></th>
                  <th><input type="text" value="<?php echo number_format($query2['uang_kembali']) ?>" class="form-control text-center" name="kembali" id="kembali" readonly></th>
                  <th></th>
                </tr>
                <tr>
                  <th colspan="3"></th>
                  <th>No Transaksi :</th>
                  <th></th>
                  <th><input type="text" value="<?php echo $query2['no_pesanan'] ?>" class="form-control text-center" name="kembali" id="kembali" readonly></th>
                  <th></th>
                </tr>
                <tr>
                  <th colspan="3"></th>
                  <th>Tanggal Transaksi :</th>
                  <th></th>
                  <th><input type="text" value="<?php echo $query2['tgl_transaksi'] ?>" class="form-control text-center" name="kembali" id="kembali" readonly></th>
                  <th></th>
                </tr>
                <tr class="bg-gray">
                  <th colspan="8">Keterangan</th>
                </tr>
                <tr class="bg-default">  
                  <th colspan="8"><textarea class="form-control"><?php echo $query2['keterangan'] ?></textarea></th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<script type="text/javascript">
  $(document).ready(function(){
    var kode = "<?php echo $query2['no_pesanan'] ?>"
    $('#hapus').click(function(){

       swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it",
        cancelButtonText: "No, cancel",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          $.ajax({
            url : base_url + 'transaksi/hapustransaksi/'+ kode,
            type : 'POST',
            cache: false,
            contentType: false,
            processData: false,
            success:function(i){

              swal({
                title: "Deleted!",
                text: "Your imaginary file has been deleted.",
                type: "success",
              }, function (isConfirm) {
           
                window.location.href = '<?php echo site_url('laporan') ?>'
              });

            }
          });
        } else {
          swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
      });
     
   })
  })
</script>
