<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-info box-solid">
        <div class="box-header">
          <h2 class="box-title"><b style="font-weight: 1000;font-variant: small-caps;font-size: 30px"> Data Transaksi </b></h2>
          <div class="box-tools">

            <button type="button" class="btn btn-warning" id="json">
              <span data-trigger="hover" data-container="body" data-toggle="popover" data-placement="left"  data-original-title="Json Data "><i class="fa fa-download"></i>&nbspJson Data
              </span>
            </button>

          </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">

          <table id="tabel" class="table table-bordered table-hover ">
            <thead>
              <tr  class="bg-info">
                <th>No</th>
                <th width="30%">No Pesanan</th>
                <th width="30%">Nama Pelanggan</th>
                <th width="30%">Nama Driver</th>
                <th width="30%">Tanggal Transaksi</th>
                <th width="20%">Total</th>
                <th width="20%">Jumlah</th>
                <th width="20%">Uang Bayar</th>
                <th width="20%">Uang Kembali</th>
                <th width="20%"></th>


              </tr>
            </thead>
            <tbody >


            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>

  <!-- /.row -->
</section>

<script type="text/javascript">
  $(document).ready(function(){
    $('#json').click(function(){
      $.ajax({
        url:"<?php echo base_url('laporan/jsondata') ?>",
        type:"POST",
        dataType:"JSON",
        success:function(){

        }

      })
    })
  })





  read()
  function read(){
    var data = '';
    <?php if (isset($pelanggan)) { ?>
      var data = "<?php  echo '?pelanggan='.$pelanggan ?>"
    <?php } ?>
    <?php if (isset($driver)) { ?>
      var data = "<?php  echo '?driver='.$driver ?>"
    <?php } ?>
    <?php if (isset($tanggal)) { ?>
      var data = "<?php  echo '?tanggal='.$tanggal ?>"
    <?php } ?>
    $.ajax({
      url:"<?php echo site_url('Laporan/read') ?>" + data,
      type:"POST",
      success:function(apa){
       $('tbody').html(apa);
       $('.table').footable({
        "paging": {
          "enabled": true
        },
        "filtering": {
          "enabled": true
        },
        "sorting": {
          "enabled": true
        },

      });

     }
   })
  }
</script>