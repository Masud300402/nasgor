<?php 
function LoadView($folder,$file,$db=NULL){
	$CI 	=& get_instance();
	$CI->load->view('skin/header',$db);
	$CI->load->view('skin/navbar',$db);
	$CI->load->view('skin/sidebar',$db);
	$CI->load->view($folder.'/'.$file,$db);
	$CI->load->view('skin/footer',$db);
	$CI->load->view('skin/control_sidebar',$db);

	
}

function view2($dollar,$db=NULL){
	$CI 	=& get_instance();
	$CI->load->view('skin/header',$db);
	$CI->load->view('skin/navbar2');
	$CI->load->view('skin/sidebar2');
	$CI->load->view($dollar.'/index.php',$db);
	$CI->load->view('skin/footer');
	$CI->load->view('skin/control_sidebar');
	

	
}

function cek_session(){
    $CI =& get_instance();
	if ($CI->session->userdata('username') == false){
		redirect('login');
	}
}

function kode($i,$var){
   $CI =& get_instance();
    $query =  $CI->db->query('SELECT CONCAT("'.$var.'", LPAD(RIGHT(IFNULL(MAX(code),0),6) +1, 6, "0")) as kode FROM '.$i.'')->row_array();
   echo $query['kode'];
}